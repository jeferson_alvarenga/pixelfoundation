# PixelFoundation

[![CI Status](https://img.shields.io/travis/Jeferson Alvarenga/PixelFoundation.svg?style=flat)](https://travis-ci.org/Jeferson Alvarenga/PixelFoundation)
[![Version](https://img.shields.io/cocoapods/v/PixelFoundation.svg?style=flat)](https://cocoapods.org/pods/PixelFoundation)
[![License](https://img.shields.io/cocoapods/l/PixelFoundation.svg?style=flat)](https://cocoapods.org/pods/PixelFoundation)
[![Platform](https://img.shields.io/cocoapods/p/PixelFoundation.svg?style=flat)](https://cocoapods.org/pods/PixelFoundation)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PixelFoundation is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PixelFoundation'
```

## Author

Jeferson Alvarenga, jeferson.alvarenga@me.com

## License

PixelFoundation is available under the MIT license. See the LICENSE file for more info.
