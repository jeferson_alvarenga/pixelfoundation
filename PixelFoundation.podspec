#
# Be sure to run `pod lib lint PixelFoundation.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PixelFoundation'
  s.version          = '0.1.0'
  s.summary          = 'A short description of PixelFoundation.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/Jeferson Alvarenga/PixelFoundation'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Jeferson Alvarenga' => 'jeferson.alvarenga@me.com' }
  s.source           = { :git => 'https://github.com/Jeferson Alvarenga/PixelFoundation.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'PixelFoundation/Classes/**/*'
  
  # s.resource_bundles = {
  #   'PixelFoundation' => ['PixelFoundation/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'PixelExtension', '~> 0.1.0'
  s.dependency 'PixelModel', '~> 0.1.0'
  s.dependency 'Alamofire', '~> 4.7'
  s.dependency 'AlamofireImage', '~> 3.3'
  s.dependency 'RxSwift', '5.0.0'
  s.dependency 'RxCocoa', '5.0.0'
  s.dependency 'FMDB', '2.7.5'
end
