//
//  SMS.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 10/08/20.
//

import Foundation
import PixelExtension
import PixelModel

public class SMS {
    
    public class func sendPinCode(user: User, onSuccess: @escaping (String)->Void) {
        let pin = getPinCode()
        #if DEBUG
            onSuccess(pin)
        #else
            let net = Network()
        let params: [String: Any] = ["to": "\(PixelConstant.SMS.countryCode)\(user.phone)",
            "content": String(format: PixelConstant.SMS.content, user.name, "\(pin)"),
                                         "from": PixelConstant.SMS.from,
                                         "dlr": "no"]
            let headers: [String: String] = ["Content-Type": PixelConstant.SMS.contentType,
                                             "Authorization": PixelConstant.SMS.authorization]
            net.request(PixelConstant.endpoint.smsSend, parameters: params, headers: headers, method:PixelConstant.SMS.requestMethod, onSuccess: { (result) in
                onSuccess(pin)
            }, onFailure: { (error) in
                //TODO log/show error
                print(error)
            })
        #endif
    }
    
    class func getPinCode() -> String {
        #if DEBUG
            return "122333"
        #else
            var result = String()
            for _ in 0..<PixelConstant.SMS.pinLengh {
                result.append("\(Int.random(in: 0..<9))")
            }
            return result
        #endif
    }
}
