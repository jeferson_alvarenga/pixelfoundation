//
//  Card.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 17/08/20.
//

import PixelModel

extension PixelConstant {
    
    public enum Cards {
        case visa
        
        public var value: Card {
            switch self {
            case .visa:
                return Card(flag: "", number: "", token: "71294e1a-342c-4d70-8a77-d88452d88884", cvv: "", limitAvailable: 0.0, isDefault: false)
            }
        }
    }
}
