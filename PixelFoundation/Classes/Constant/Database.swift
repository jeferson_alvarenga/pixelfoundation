//
//  Database.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 23/08/20.
//

extension PixelConstant {
    
    public struct Database {
        static public let directoryPath = FileManager.SearchPathDirectory.documentDirectory
        static public let fileName = ".database.sqlite"
        static public let dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
    }
}
