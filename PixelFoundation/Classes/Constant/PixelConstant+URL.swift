//
//  PixelURL.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 11/08/20.
//

extension PixelConstant {
    
    public enum URL: String {
        case api = "https://pixelpay.azurewebsites.net/v1"
        //case api = "http://192.168.0.19:5002/v1"
        case apiToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE1OTc3NzA3NTEsImV4cCI6MTYyOTMwNjc1MSwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsIkdpdmVuTmFtZSI6IkpvaG5ueSIsIlN1cm5hbWUiOiJSb2NrZXQiLCJFbWFpbCI6Impyb2NrZXRAZXhhbXBsZS5jb20iLCJSb2xlIjpbIk1hbmFnZXIiLCJQcm9qZWN0IEFkbWluaXN0cmF0b3IiXX0.UvcmzP98dyZoMvYBvSFVOAelwOfoL25s0RE4c7nEmzw"
        case apiDateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
    }
    
    public enum Request: Int {
        case httpMaxRetryAttempts = 2
    }
    
    public enum endpoint {
        
        case insertUser
        case smsSend
        case userRecoverPassword(user: String)
        
        public var path: String {
            switch self {
            case .insertUser:
                return "\(PixelConstant.URL.api.rawValue)/user"
            case .userRecoverPassword(let documentNumber):
                return "\(PixelConstant.URL.api.rawValue)/user/\(documentNumber)/recover"
            case .smsSend:
                return "https://rest-api.d7networks.com/secure/send"
            }
        }
    }
}
