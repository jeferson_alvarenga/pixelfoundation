//
//  PixelConstant.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 11/08/20.
//

public class PixelConstant {
    
    public enum PaymentGateway: String {
        case merchantID = "317e99a0-faef-40d7-96df-d36a82f69563"
        case merchantKey = "XPSPEDBQPCXZYPSSEBINEITQPUXSHBZPDVOVYSWR"
    }
    
    public struct SMS {
        static let contentType = "application/x-www-form-urlencoded"
        static let authorization: String = "Basic bmh4Zzc0MDg6UGUwU0lJQW4="
        static let url = "https://rest-api.d7networks.com/secure/send"
        static let requestMethod = "POST"
        public static let pinLengh: Int = 6
        static let content = "Pixel Pay | Olá %@\nUtilize o código %@ para iniciar"
        static let from = "Pixel"
        static let countryCode = "+55"
    }
    
    public struct Default {
        public static let  keychainService = "PixelPay"
    }
}
