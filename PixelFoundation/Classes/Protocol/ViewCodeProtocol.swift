//
//  ViewCodeProtocol.swift
//  PixelPayment
//
//  Created by Jeferson Alvarenga on 18/07/20.
//

protocol ViewCodeProtocol: class {
    func viewCodeSetup()
    func hierarchySetup()
    func constraintSetup()
    func themeSetup()
    func additionalSetup()

}

extension ViewCodeProtocol {
    
    func viewCodeSetup() {
        hierarchySetup()
        constraintSetup()
        themeSetup()
        additionalSetup()
    }
    
    func themeSetup() {}
    
    func additionalSetup() {}
}
