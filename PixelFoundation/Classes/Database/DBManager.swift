//
//  DBManager.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 23/08/20.
//

import Foundation
import FMDB

public enum DBManagerError: Error {
    case initialization
    case open
    case transaction
}

open class DBManager {
    
    public let database: FMDatabase
    
    public init(directoryPath: FileManager.SearchPathDirectory = PixelConstant.Database.directoryPath) throws {
        let databasePathDirectory = (NSSearchPathForDirectoriesInDomains(directoryPath, .userDomainMask, true)[0])
        let pathToDatabase = databasePathDirectory.appending("/\(PixelConstant.Database.fileName)")
        if FileManager.default.fileExists(atPath: pathToDatabase) {
            database = FMDatabase(path: pathToDatabase)
            print(database.databasePath ?? "")
        } else {
            guard let dbScriptPath = Bundle.main.path(forResource: "DBScript", ofType: "txt") else {
                throw DBManagerError.initialization
            }
            do {
                let content = try String(contentsOfFile: dbScriptPath, encoding: .utf8)
                let queries = content.components(separatedBy: "[!]")
                
                if !FileManager.default.fileExists(atPath: databasePathDirectory) {
                    try FileManager.default.createDirectory(at: URL(fileURLWithPath: databasePathDirectory), withIntermediateDirectories: false, attributes: nil)

                }
                database = FMDatabase(path: pathToDatabase)
                #if DEBUG
                    print("DatabasePath: \(database.databasePath ?? "")")
                #endif
                try openDatabase()
                defer {
                    database.close()
                }
                for query in queries where query.isNotEmpty {
                    try database.executeUpdate(query, values: nil)
                }
                if let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                    UserDefaultsUtils.set(value: appVersion, forKey: .lastDatabaseVersion)
                }
                UserDefaultsUtils.set(value: true, forKey: .hasPendingSync)
            } catch {
                throw DBManagerError.initialization
            }
        }
    }
    
    public func openDatabase() throws {
        if !database.open() {
            throw DBManagerError.open
        }
    }
    
    public func destroyIfNeeded(fileName: String = PixelConstant.Database.fileName) throws {
        let databasePathDirectory = (NSSearchPathForDirectoriesInDomains(PixelConstant.Database.directoryPath, .userDomainMask, true)[0])
        let pathToDatabase = databasePathDirectory.appending("/\(fileName)")
        if let lastDatabaseVersion = UserDefaultsUtils.get(forKey: .lastDatabaseVersion) as? String,
            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            if lastDatabaseVersion.compare(appVersion, options: .numeric) != .orderedSame {
                if FileManager.default.fileExists(atPath: pathToDatabase) {
                    try FileManager.default.removeItem(atPath: pathToDatabase)
                }
            }
        } else {
            if FileManager.default.fileExists(atPath: pathToDatabase) {
                try FileManager.default.removeItem(atPath: pathToDatabase)
            }
        }
    }
}
