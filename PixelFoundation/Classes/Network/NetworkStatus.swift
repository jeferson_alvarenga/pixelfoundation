//
//  NetworkStatus.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 10/08/20.
//

import RxSwift
import RxCocoa

enum NetworkState {
    case standBy
    case inProgress
    case finished
    case error
}

class NetworkStatus {
    
    static let shared = NetworkStatus()
    var status: BehaviorRelay<NetworkState> = BehaviorRelay(value: .standBy)
    
    init() {
        
    }
    
    func reset() {
        status.accept(.finished)
        status.accept(.standBy)
    }
}
