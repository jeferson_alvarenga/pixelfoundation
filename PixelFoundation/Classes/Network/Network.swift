//
//  Network.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 10/08/20.
//

import Foundation
import Alamofire
import AlamofireImage
import RxCocoa

//TODO https://stackoverflow.com/questions/39985856/getting-client-certificate-to-work-for-mutual-authentication-using-swift-3-and-a

public enum NetworkError: Error {
    case response(statusCode: Int?, message: String?, error: Error?)
    case parse
    case unknown
}

public enum APIEndpoint: String {
    case base = "https://my.api.mockaroo.com/"
    case signin = "signin"
    case signup = "https://www.mocky.io/v2/5bc4b4153000006600758b21"
    case businessUnits = "business_unit.json?key=eb17f360"
    case products = "products.json?key=eb17f360"
    case paymentConditions = "paymentcondition.json?key=eb17f360"
    case clients = "clients.json?key=eb17f360"
    case inventories = "inventory.json?key=eb17f360"
    case Warehouses = "warehouse.json?key=eb17f360"
    case prices = "prices.json?key=eb17f360"
    case measureUnits = "measureunits.json?key=eb17f360"
    case clientGroup = "clientgroup.json?key=eb17f360"
    case clientRegion = "clientregion.json?key=eb17f360"
    case productRange = "productrange.json?key=eb17f360"
    case sale = "sale.json?key=eb17f360"
    case saleProduct = "saleproduct.json?key=eb17f360"
    case clientContact = "clientcontact.json?key=eb17f360"
    case clientLocation = "clientlocation.json?key=eb17f360"
    
    var absoluteURL: String {
        return APIEndpoint.base.rawValue + self.rawValue
    }
    
    func composedURL(_ sufix: String) -> String {
        return APIEndpoint.base.rawValue + self.rawValue + sufix
    }
}

public class Network {
    public typealias onFailure = (NetworkError) -> Void
    var alamoFireManager: Alamofire.SessionManager!
    var backgroundCompletionHandler: (() -> Void)? {
        get {
            return alamoFireManager.backgroundCompletionHandler
        }
        set {
            alamoFireManager.backgroundCompletionHandler = newValue
        }
    }
    
    public init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 200
        configuration.timeoutIntervalForResource = 200
        alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        NetworkStatus.shared.status.accept(.inProgress)
    }
    
    public func request<T: Codable>(_ endpoint: PixelConstant.endpoint,
                               entity: T.Type,
                               parameters: Any? = nil,
                               headers: [String: String]? = nil,
                               method: String? = "GET",
                               onSuccess: @escaping (T) -> Void,
                               onFailure: @escaping onFailure) {
        
        if !checkInternetConnection() {
            onFailure(NetworkError.response(statusCode: 0, message: nil, error: nil))
        } else {
            do {
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                }
                var request = URLRequest(url: try endpoint.path.asURL())
                var httpMethod = HTTPMethod.get
                
                //Parameters
                if let params = parameters {
                    request.httpBody = try JSONSerialization.data(withJSONObject: params)
                }
                
                //Headers
                if let unwrapedHeaders = headers {
                    for header in unwrapedHeaders {
                        request.setValue(header.value, forHTTPHeaderField: header.key)
                    }
                } else {
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                }
                
                //Method
                if let parameterMethod = method, let vMethod = HTTPMethod(rawValue: parameterMethod) {
                    httpMethod = vMethod
                }
                request.httpMethod = httpMethod.rawValue
                alamoFireManager.request(request).validate().responseDecodable { (response: DataResponse<T>) in
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                    if response.result.isSuccess {
                        if let value = response.result.value {
                            onSuccess(value as T)
                        } else {
                            onFailure(NetworkError.unknown)
                        }
                    } else {
                        onFailure(NetworkError.response(statusCode: response.response?.statusCode, message: response.description, error: nil))
                    }
                }.session.finishTasksAndInvalidate()
            } catch {
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                onFailure(NetworkError.response(statusCode: (error as NSError).code, message: (error as NSError).localizedDescription, error: error))
            }
        }
    }
    
    /*func mappables<T: BaseMappable>(_ url: String,
                                entity: T.Type,
                                parameters: Any? = nil,
                                method: String? = "GET",
                                onSuccess: @escaping ([T]) -> Void,
                                onFailure: @escaping onFailure) {
        
        if !checkInternetConnection() {
                onFailure(OneSaleError (type: .noInternetConnection, title: .noInternetConnectionError))
        } else {
            do {
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                }
                var request = URLRequest(url: try url.asURL())
                var httpMethod = HTTPMethod.get
                
                //Headers
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                
                //Parameters
                if let params = parameters {
                    request.httpBody = try JSONSerialization.data(withJSONObject: params)
                }
                
                //Method
                if let parameterMethod = method, let vMethod = HTTPMethod(rawValue: parameterMethod) {
                    httpMethod = vMethod
                }
                request.httpMethod = httpMethod.rawValue
                alamoFireManager.request(request).validate().responseArray { (response: DataResponse<[T]>) in
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                    if response.result.isSuccess {
                        if let value = response.result.value {
                            onSuccess(value as [T])
                            NetworkStatusManager.shared.reset()
                        } else {
                            onFailure(OneSaleError(type: .request(statusCode: response.response?.statusCode), title: .invalidServerResponseError))
                            NetworkStatusManager.shared.status.value = .error
                        }
                    } else {
                        onFailure(OneSaleError(type: .request(statusCode: response.response?.statusCode), title: .invalidServerResponseError))
                        NetworkStatusManager.shared.status.value = .error
                    }
                }.session.finishTasksAndInvalidate()
            } catch {
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                onFailure(OneSaleError(type: .request(statusCode: (error as NSError).code), title: .invalidHTTPRequestError, error: error))
                NetworkStatusManager.shared.status.value = .error
            }
        }
    }
    
    func request(_ url: String, onSuccess: @escaping (Image) -> Void){
        alamoFireManager.request(url).responseImage(imageScale: 1, inflateResponseImage: false, completionHandler: {response in
            guard let image = response.result.value else {
                return
            }
            DispatchQueue.main.async {
                onSuccess(image)
            }
        })
    }*/
    
    public func request(_ endpoint: PixelConstant.endpoint,
                 parameters: Any? = nil,
                 headers: [String: String]? = nil,
                 method: String? = "GET",
                 onSuccess: @escaping (Any) -> Void,
                 onFailure: @escaping onFailure) {
        
        if !checkInternetConnection() {
            onFailure(NetworkError.response(statusCode: nil, message: nil, error: nil))
        } else {
            do {
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = true
                }
                var request = URLRequest(url: try endpoint.path.asURL())
                var httpMethod = HTTPMethod.get
                
                //Headers
                if let unwrapedHeaders = headers {
                    for header in unwrapedHeaders {
                        request.setValue(header.value, forHTTPHeaderField: header.key)
                    }
                } else {
                    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                }
                
                //Parameters
                if let params = parameters {
                    request.httpBody = try JSONSerialization.data(withJSONObject: params)
                }
                
                //Method
                if let parameterMethod = method, let vMethod = HTTPMethod(rawValue: parameterMethod) {
                    httpMethod = vMethod
                }
                request.httpMethod = httpMethod.rawValue
                alamoFireManager.request(request).validate().responseJSON { (response) in
                    NetworkStatus.shared.reset()
                    DispatchQueue.main.async {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    }
                    if response.result.isSuccess {
                        if let value = response.result.value {
                            onSuccess(value)
                        } else {
                            onFailure(NetworkError.response(statusCode: response.response?.statusCode, message: response.description, error: nil))
                        }
                    } else {
                        onFailure(NetworkError.response(statusCode: response.response?.statusCode, message: response.description, error: nil))
                    }
                }.session.finishTasksAndInvalidate()
            } catch {
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
                onFailure(NetworkError.response(statusCode: (error as NSError).code, message: (error as NSError).localizedDescription, error: error))
            }
        }
    }
    
    func checkInternetConnection() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

extension DataRequest {
    
    private func responseObject<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            
            return Result { try JSONDecoder().decode(T.self, from: data) }
        }
    }
    
    @discardableResult
    func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: responseObject(), completionHandler: completionHandler)
    }
    
    /// @Returns - DataRequest
    /// completionHandler handles JSON Array [T]
    @discardableResult
    func responseCollection<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
       let responseSerializer = DataResponseSerializer<[T]>{ request, response, data, error in
           guard error == nil else {return .failure(error!)}

           let result = DataRequest.serializeResponseData(response: response, data: data, error: error)
           guard case let .success(jsonData) = result else {
               return .failure(error!)
           }
           
           let decoder = JSONDecoder()
           guard let responseArray = try? decoder.decode([T].self, from: jsonData) else {
            return .failure(NetworkError.parse)
           }
           return .success(responseArray)
       }
       return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
   }
}
