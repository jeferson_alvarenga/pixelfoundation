//
//  UserDefaultUtils.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 23/08/20.
//

import Foundation

enum UserDefaultsKey: String {
    case setupCompleted
    case databaseUpdateRequired
    case lastSyncDate
    case hasPendingSync
    case lastDatabaseVersion
    case account
}

extension UserDefaultsKey {
    
    func exists() -> Bool {
        return UserDefaultsUtils.has(key: self)
    }
    
    func destroy() {
        UserDefaultsUtils.remove(forKey: self)
    }
}

class UserDefaultsUtils {
    
    class func has(key: UserDefaultsKey) -> Bool {
        return get(forKey: key.rawValue) != nil
    }
    
    class func get(forKey: UserDefaultsKey) -> Any? {
        return get(forKey: forKey.rawValue)
    }
    
    class func set(value: Any, forKey: UserDefaultsKey, sync: Bool? = false) {
        return set(value: value, forKey: forKey.rawValue)
    }
    
    class func reset() {
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
    }
    
    class func synchronize() {
        UserDefaults.standard.synchronize()
    }
    
    class func remove(forKey: UserDefaultsKey) {
        remove(forKey: forKey.rawValue)
    }
    
    private class func remove(forKey: String) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: forKey)
    }
    
    private class func set(value: Any, forKey: String, sync: Bool? = false) {
        let defaults = UserDefaults.standard
        if defaults.object(forKey: forKey) == nil {
            update(value: value, forKey: forKey)
        }else{
            insert(value: value, forKey: forKey)
        }
        if let sync = sync, sync {
            UserDefaultsUtils.synchronize()
        }
    }
    
    private class func get(forKey: String) -> Any? {
        let defaults = UserDefaults.standard
        return defaults.object(forKey: forKey)
    }
    
    private class func insert(value: Any, forKey: String) {
        let defaults = UserDefaults.standard
        defaults.setValue(value, forKey: forKey)
    }
    
    private class func update(value: Any, forKey: String) {
        remove(forKey: forKey)
        insert(value: value, forKey: forKey)
    }
}
