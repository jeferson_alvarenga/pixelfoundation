//
//  CountDownTimer.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 11/08/20.
//

import Foundation

public protocol CountDownTimerDelegate: class {
    func onCountDownTimerTick(time: Int, formatted: String)
    func onCountDownTimerComplete()
}

public enum CountDownTimerType {
    case hourMinSec
    case `default`
}

public class CountDownTimer: NSObject {
    
    var timer: Timer?
    var totalTime: Int
    var type: CountDownTimerType = .default
    var timeLapsed: Int = 0
    weak var delegate: CountDownTimerDelegate?
    
    public init(delegate: CountDownTimerDelegate, totalTime: Int) {
        self.delegate = delegate
        self.totalTime = totalTime
    }
    
    public func start() {
        timeLapsed = 0
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTimer() {
        if timeLapsed < totalTime {
            timeLapsed += 1  // decrease counter timer
            delegate?.onCountDownTimerTick(time: timeLapsed, formatted: timeFormatted()) // will show timer
        } else {
            timer?.invalidate()
            timer = nil
            delegate?.onCountDownTimerComplete()
        }
    }
    func timeFormatted() -> String {
        let timeDiff = totalTime - timeLapsed
        let seconds: Int = timeDiff % 60
        let minutes: Int = (timeDiff / 60) % 60
        switch type {
        case .hourMinSec:
            let hours: Int = ((timeDiff / 60) / 60) % 60
            return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
        default:
            return String(format: "%02d:%02d", minutes, seconds)
        }
    }
}
