//
//  SessionUtils.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 24/08/20.
//

import PixelModel

public enum SessionKey:String {
    case user
    case isSignedIn
}

public class SessionUtils {
    
    static var data: [String: Any] = [:]
    
    public static func set(key: SessionKey, value: Any) {
        data[key.rawValue] = value
    }
    
    public static func get(_ key: SessionKey) -> Any? {
        return data[key.rawValue]
    }
    
    public static func delete(key: SessionKey) {
        data.removeValue(forKey: key.rawValue)
    }
}

extension SessionUtils {
    
    public static func selectUser() throws -> User? {
        let worker = try SessionWorker()
        return try worker.selectUser()
    }
    
    public static func selectLastOnboardingUser() throws -> User? {
        let worker = try SessionWorker()
        return try worker.selectOnboardingUser()
    }
}
