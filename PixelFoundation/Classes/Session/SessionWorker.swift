//
//  SessionWorker.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 25/08/20.
//

import PixelModel

class SessionWorker: DBManager {
    
    func selectUser() throws -> User {
        try openDatabase()
        defer {
            database.close()
        }
        let resultSet = try database.executeQuery("SELECT * FROM User ORDER BY userID DESC LIMIT 1", values: nil)
        while resultSet.next() {
            let userID = resultSet.string(forColumn: "userID") ?? ""
            let name = resultSet.string(forColumn: "name") ?? ""
            let lastName = resultSet.string(forColumn: "lastName") ?? ""
            let phone = resultSet.string(forColumn: "phone") ?? ""
            let documentNumber = resultSet.string(forColumn: "documentNumber") ?? ""
            let passphrase = resultSet.string(forColumn: "passphrase") ?? ""
            return User(userID: userID, name: name, lastName: lastName, phone: phone, documentNumber: documentNumber, passphrase: passphrase)
        }
        return User()
    }
    
    func selectOnboardingUser() throws -> User {
        try openDatabase()
        defer {
            database.close()
        }
        let resultSet = try database.executeQuery("SELECT * FROM OnboardingUser ORDER BY userID DESC LIMIT 1", values: nil)
        while resultSet.next() {
            let userID = resultSet.string(forColumn: "userID") ?? ""
            let name = resultSet.string(forColumn: "name") ?? ""
            let lastName = resultSet.string(forColumn: "lastName") ?? ""
            let phone = resultSet.string(forColumn: "phone") ?? ""
            let documentNumber = resultSet.string(forColumn: "documentNumber") ?? ""
            let passphrase = resultSet.string(forColumn: "passphrase") ?? ""
            return User(userID: userID, name: name, lastName: lastName, phone: phone, documentNumber: documentNumber, passphrase: passphrase)
        }
        return User()
    }
}
