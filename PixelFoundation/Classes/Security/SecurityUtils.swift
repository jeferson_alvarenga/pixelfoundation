//
//  SecurityUtils.swift
//  PixelFoundation
//
//  Created by Jeferson Alvarenga on 24/08/20.
//

public class SecurityUtils {
    
    static let service: String = PixelConstant.Default.keychainService
    static var passwordItem: KeychainPasswordItem?
    
    //TODO Use associated domain, autofill https://medium.com/@abhimuralidharan/password-autofill-for-ios-apps-for-faster-login-ios-11-1d9f77deb35a
    public class func initKeychain() {
        if let userDefaultAccountHashed = UserDefaultsUtils.get(forKey: UserDefaultsKey.account) as? String,
            let keychainItem = try? KeychainPasswordItem.passwordItems(forService: service).filter({$0.account == userDefaultAccountHashed}).first {
            passwordItem = keychainItem
        }
    }
    
    public class var hasPassword: Bool {
        if let password = try? passwordItem?.readPassword(), password.count > 0 {
            return true
        }
        return false
    }
    
    public class func savePassword(account: String, password: String) throws {
        let hashableAccount = "\(account.hashValue)"
        if passwordItem == nil {
            passwordItem = KeychainPasswordItem(service: service, account: hashableAccount)
            UserDefaultsUtils.set(value: hashableAccount, forKey: .account)
        } else {
            try passwordItem?.renameAccount(hashableAccount)
            UserDefaultsUtils.set(value: hashableAccount, forKey: .account)
        }
        try passwordItem?.savePassword("\(password)")
    }
    
    public class func authenticate(password: String) throws -> Bool {
        let keychainPassword = try passwordItem?.readPassword()
        return password == keychainPassword
    }
    
    class func getHashableAccount(_ account: String) -> String? {
        if let userDefaultAccountHashed = UserDefaultsUtils.get(forKey: UserDefaultsKey.account) as? String {
            return userDefaultAccountHashed
        } else {
            return nil
        }
    }
}
